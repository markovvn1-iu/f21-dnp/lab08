import argparse
import time
import random
from enum import Enum
from dataclasses import dataclass
from typing import Any, Callable, Dict, Generator, List, Optional, Tuple
from threading import Thread, Semaphore
from contextlib import contextmanager
from xmlrpc.server import SimpleXMLRPCServer

from proxy import RAFTServerProxy


class MalformedConfigException(Exception):
    pass


@dataclass(frozen=True)
class ServerConfig:
    id: int
    address: str
    port: int

    @property
    def http_adderess(self) -> str:
        return f'http://{self.address}:{self.port}'


class ServerState(Enum):
    FOLLOWER = 'follower'
    CANDIDATE = 'candidate'
    LEADER = 'leader'


def parse_config_file(file_name: str) -> List[ServerConfig]:
    with open(file_name, 'r') as f:
        lines = f.read().split('\n')

    res: List[ServerConfig] = []
    for line_no, line in enumerate(map(lambda x: x.strip(), lines)):
        if line.startswith('#') or len(line) == 0:
            continue
        line_parts = line.split()
        if len(line_parts) != 3:
            raise MalformedConfigException(f'Line {line_no+1}: config info has {len(line_parts)} parts but must have 3')
        try:
            res_id = int(line_parts[0])
        except ValueError as e:
            raise MalformedConfigException(f'Line {line_no+1}: config info has incorrect id (cannot convert id to int)') from e
        
        res_addr = line_parts[1]

        try:
            res_port = int(line_parts[2])
        except ValueError as e:
            raise MalformedConfigException(f'Line {line_no+1}: config info has incorrect port (cannot convert port to int)') from e

        res.append(ServerConfig(res_id, res_addr, res_port))

    if len({i.id for i in res}) != len(res):
        raise MalformedConfigException('config info has two or more servers with same id')

    return res


class RAFTServerTimer(Thread):
    "Thread timer with accuracy ~80μs on Linux"

    _working = False

    _init_timer_ns: int
    _last_responce_time_ns: int
    _timeisup_callback: Optional[Callable[[], None]] = None
    _called: bool = False

    def __init__(self, init_timer_ns: int, timeisup_callback: Callable[[], None]) -> None:
        super().__init__()
        self._init_timer_ns = init_timer_ns
        self._timeisup_callback = timeisup_callback

    def reset_timer(self) -> None:
        self._last_responce_time_ns = time.monotonic_ns()
        self._called = False

    def wait_left_ns(self) -> int:
        return self._init_timer_ns - (time.monotonic_ns() - self._last_responce_time_ns)

    def run(self) -> None:
        self._working = True
        self.reset_timer()

        while self._working:
            wait_left_ns = self.wait_left_ns()
            to_wait = 0
            if wait_left_ns > 0:
                to_wait = wait_left_ns / 1.2e9
            elif self._called:
                to_wait = self._init_timer_ns / 1.2e9
            
            if to_wait > 0:
                time.sleep(to_wait)
                continue

            self._called = True
            self._timeisup_callback and self._timeisup_callback()

    def stop(self) -> None:
        self._working = False


class RAFTServerThreadQueue(Thread):

    _working: bool = False

    _tasks: Semaphore
    _queue: List[Callable[[], None]] = []

    def __init__(self) -> None:
        super().__init__()
        self._tasks = Semaphore()
        self._tasks.acquire()

    def add_task(self, func: Callable[[], None]) -> None:
        self._queue.append(func)
        self._tasks.release()

    def run(self) -> None:
        self._working = True

        while self._working:
            self._tasks.acquire()
            if not self._working:
                break
            self._queue.pop(0)()

    def stop(self) -> None:
        self._working = False
        self._tasks.release()


class RAFTServer(Thread):

    _cfg: ServerConfig
    _raft_cfg: Dict[int, ServerConfig]
    _other_servers: Dict[int, RAFTServerProxy]

    _working: bool = False
    _server: SimpleXMLRPCServer

    _end_suspend: int = 0
    _queue: RAFTServerThreadQueue
    _timer: RAFTServerTimer
    _leader_timer: RAFTServerTimer

    _leader_id: int = -1
    _last_vote_term: int = -1  # the tearm in which the server last voted
    _term: int = -1
    _state: ServerState = ServerState.FOLLOWER

    @property
    def state(self) -> ServerState:
        return self._state

    def _get_server_cfg(self, server_id: int) -> ServerConfig:
        if server_id not in self._raft_cfg:
            raise RuntimeError(f'Config for server with id {server_id} not found')
        return self._raft_cfg[server_id]

    def __init__(self, server_id: int, raft_cfg: List[ServerConfig]) -> None:
        super().__init__()
        self._raft_cfg = {i.id: i for i in raft_cfg}
        self._cfg = self._get_server_cfg(server_id)

        self._other_servers = {i.id: RAFTServerProxy(i.http_adderess) for i in raft_cfg if i.id != server_id}

        self._queue = RAFTServerThreadQueue()
        init_timer = random.randint(150_000_000, 300_000_000)
        self._timer = RAFTServerTimer(init_timer, self._timer_is_up_callback)
        self._leader_timer = RAFTServerTimer(50_000_000, self._leader_timer_is_up_callback)

        self._set_server_state_term(0, ServerState.FOLLOWER)

        self._server = SimpleXMLRPCServer((self._cfg.address, self._cfg.port), logRequests=False)
        self._server.timeout = 0.01
        self._server.register_introspection_functions()
        self._server.register_function(self.request_vote)
        self._server.register_function(self.append_entries)
        self._server.register_function(self.get_leader)
        self._server.register_function(self.suspend)

    def _set_server_state_term(self, new_term: Optional[int], new_state: Optional[ServerState] = None) -> None:
        term = new_term if new_term is not None else self._term
        state = new_state if new_state is not None else self._state

        if term == self._term and self._state == state:
            return

        self._term = term
        self._state = state
        print(f'I am a {self._state.value}. Term: {self._term}')

    def request_vote(self, term: int, candidate_id: int) -> Tuple[int, bool]:
        """Request vote

        This function is called by the Candidate during the elections to collect votes

        Args:
            term (int): candidate's term number
            candidate_id (int): id of a candidate

        Returns:
            Tuple[int, bool]: term number of the server, result of voting (True/False)
        """
        self._timer.reset_timer()

        if candidate_id not in self._raft_cfg:
            print(f'Unknown candidate_id {candidate_id}')
            return self._term, False

        if term < self._term:
            return self._term, False

        if self._last_vote_term == term:
            self._set_server_state_term(term)
            return self._term, False

        self._set_server_state_term(term, ServerState.FOLLOWER)
        self._leader_id = candidate_id  # hope this candidate will become a leader
        self._last_vote_term = self._term
        print(f'Voted for node {candidate_id}')
        return self._term, True

    def append_entries(self, term: int, leader_id: int) -> Tuple[int, bool]:
        """Recive heartbeat messages and replicate the log

        In this lab, this function is only used to send heartbeat messages. The name is
        left unchanged to understand that the same function is used to replicate the log.

        Args:
            term (int): current term number from the leader
            leader_id (int): leader's id. So that the follower knows who his leader is.

        Returns:
            Tuple[int, bool]: term number of the server, success (True/False)
        """
        self._timer.reset_timer()

        if leader_id not in self._raft_cfg:
            print(f'Unknown leader_id {leader_id}')
            return self._term, False

        if term < self._term:
            return self._term, False

        self._leader_id = leader_id
        self._set_server_state_term(term, ServerState.FOLLOWER)
        return self._term, True

    def get_leader(self) -> Tuple[int, str]:
        """Returns the current Leader id and address

        This function is called by the client.

        Returns:
            Tuple[int, str]: leader id and address or (-1, '') if there is no leader
        """
        if self._leader_id not in self._raft_cfg:
            return -1, ''
        cfg = self._raft_cfg[self._leader_id]
        return cfg.id, cfg.http_adderess

    def suspend(self, period: float) -> bool:
        """Makes the server sleep for period seconds

        Used to simulate a short-term disconnection of the server from the system.
        This function is called by the client.

        Args:
            period (float): period server should sleep for

        Returns:
            bool: success (True/False)
        """
        print(f'Sleeping for {period} seconds')
        # Stop heartbeat for 'periond' seconds
        self._end_suspend = time.monotonic_ns() + int(period * 1e9)
        return True

    def _run_elections(self) -> None:
        votes_count = 1

        # TODO: Если они отвечают слишком долго, то нужно прекратить
        for server in self._other_servers.values():
            try:
                server_term, vote = server.request_vote(self._term, self._cfg.id)
            except ConnectionRefusedError:
                continue
            if server_term > self._term:
                self._set_server_state_term(server_term, ServerState.FOLLOWER)
                return
            votes_count += vote

        if votes_count * 2 > len(self._raft_cfg):
            print('Votes received')
            self._set_server_state_term(None, ServerState.LEADER)
            self._leader_id = self._cfg.id
        else:
            print('Votes not received')
            self._set_server_state_term(None, ServerState.FOLLOWER)

    def _run_heartbeat(self) -> None:
        for server in self._other_servers.values():
            try:
                term, status = server.append_entries(self._term, self._cfg.id)
            except ConnectionRefusedError:
                continue
            if term > self._term:
                self._set_server_state_term(term, ServerState.FOLLOWER)
                return

    def _timer_is_up_callback(self):
        self._timer.reset_timer()

        if self._state != ServerState.FOLLOWER:
            return

        print('The leader is dead')
        self._set_server_state_term(self._term + 1, ServerState.CANDIDATE)

        self._last_vote_term = self._term
        print(f'Voted for node {self._cfg.id}')

        self._queue.add_task(self._run_elections)

    def _leader_timer_is_up_callback(self):
        to_wait = self._end_suspend - time.monotonic_ns()
        if to_wait > 0:
            time.sleep(to_wait / 1e9)

        self._leader_timer.reset_timer()
        if self._state == ServerState.LEADER:
            self._queue.add_task(self._run_heartbeat)

    def run(self):
        self._queue.start()
        self._timer.start()
        self._leader_timer.start()

        print(f'Server is started at {self._cfg.address}:{self._cfg.port}')

        self._timer.reset_timer()

        self._working = True
        while self._working:
            self._server.handle_request()

        self._server.server_close()

    def stop(self):
        self._timer.stop()
        self._leader_timer.stop()
        self._queue.stop()
        self._working = False

    def join(self):
        self._timer.join()
        self._leader_timer.join()
        self._queue.join()
        super().join()


def parse_args(args=None):
    """Parse command line arguments."""
    parser = argparse.ArgumentParser(
        description='Simple implementation of the Chord protocol (D&NP lab06)',
        add_help=True,
    )

    parser.add_argument('id', type=int, metavar='ID', help='id of the server')
    parser.add_argument('-config-file', type=str, metavar='FILE_NAME', default='config.conf', help='path to the config file')
    return parser.parse_args(args)


def main() -> None:
    args = parse_args()
    raft_cfg = parse_config_file(args.config_file)

    server = RAFTServer(args.id, raft_cfg)
    server.start()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        print('\nKeyboardInterrupt')
    finally:
        server.stop()
        server.join()

if __name__ == '__main__':
    main()
