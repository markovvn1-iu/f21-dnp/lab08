import urllib
from xmlrpc.client import ServerProxy, Transport
from typing import Any, Dict, List, Optional, Tuple


class TimeoutTransport(Transport):

    timeout: Optional[float] = None

    def __init__(self, timeout=None, use_datetime=False, use_builtin_types=False,
                 *, headers=()):
        super().__init__(use_datetime, use_builtin_types, headers=headers)
        self.timeout = timeout

    def make_connection(self, host):
        connection = super().make_connection(host)
        if self.timeout is not None:
            connection.timeout = self.timeout
        return connection


class TimeoutServerProxy:

    _transport: TimeoutTransport
    _proxy: ServerProxy

    def __init__(self, uri, encoding=None, verbose=False,
                 allow_none=False, use_datetime=False, use_builtin_types=False,
                 *, headers=()):
        type, uri = urllib.splittype(uri)
        if type not in ("http"):
            raise OSError("unsupported XML-RPC protocol")
        self._transport = TimeoutTransport(use_datetime, use_builtin_types, headers=headers)
        self._proxy = ServerProxy(uri, self._transport, encoding, verbose, allow_none)

    @property
    def timeout(self) -> Optional[float]:
        return self._transport.timeout

    @timeout.setter
    def timeout(self, value: Optional[float]):
        self._transport.timeout = value


class MyServerProxy:

    _proxy: ServerProxy

    def __init__(self, *args: Any, **kwargs: Any):
        self._proxy = ServerProxy(*args, **kwargs)


class RAFTServerProxy(MyServerProxy):
    """Proxy for the RAFTServer
    
    See RAFTServer class for more details
    """
    def request_vote(self, term: int, candidate_id: int) -> Tuple[int, bool]:
        return self._proxy.request_vote(term, candidate_id)

    def append_entries(self, term: int, leader_id: int) -> Tuple[int, bool]:
        return self._proxy.append_entries(term, leader_id)

    def get_leader(self) -> Tuple[int, str]:
        return self._proxy.get_leader()

    def suspend(self, period: float) -> bool:
        return self._proxy.suspend(period)
