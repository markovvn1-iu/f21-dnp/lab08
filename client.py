import argparse
import time
import random
from enum import Enum
from dataclasses import dataclass
from typing import Any, Callable, Dict, Generator, List, Optional, Tuple
from threading import Thread, Semaphore
from contextlib import contextmanager
from xmlrpc.server import SimpleXMLRPCServer

from proxy import RAFTServerProxy


class RAFTClient:

    _server: Optional[RAFTServerProxy] = None

    @property
    def server(self) -> RAFTServerProxy:
        if self._server is None:
            raise RuntimeError('Not connected to any server. Use connect command to connect to a server')
        return self._server

    def connect(self, address: str, port: int):
        self._server = RAFTServerProxy(f'http://{address}:{port}')

    def get_leader(self) -> Tuple[int, str]:
        return self.server.get_leader()

    def suspend(self, period: float) -> None:
        self.server.suspend(period)

    def process_user_input(self, cmdline: str) -> bool:
        cmd = cmdline.split()
        if not cmd:
            return True
        if cmd[0] == 'connect' and len(cmd) == 3:
            self.connect(cmd[1], int(cmd[2]))
        elif cmd[0] == 'getleader' and len(cmd) == 1:
            id, address = self.get_leader()
            print(f'{id} {address}')
        elif cmd[0] == 'suspend' and len(cmd) == 2:
            self.suspend(float(cmd[1]))
        elif cmd[0] == 'quit' and len(cmd) == 1:
            return False
        else:
            raise ValueError(f'Unknown command. Try again')

        return True



def main() -> None:
    client = RAFTClient()
    print('The client starts')

    try:
        while True:
            try:
                if not client.process_user_input(input('> ')):
                    break
            except EOFError:
                print('\nEOFError')
                break
            except Exception as e:
                print(repr(e))
    except KeyboardInterrupt:
        print('\nKeyboardInterrupt')

    print('The client ends')

if __name__ == '__main__':
    main()
